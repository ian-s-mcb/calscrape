#!/usr/bin/env python
'''
calscrape.py

Command line interface for the calscrape tool. See the subdir calscrape
for the parse and upload logic invoked by this interface.
'''
import argparse

from calscrape import parse, upload, combined

def handle_arguments():

    parser = argparse.ArgumentParser()
    parser.add_subparsers()

    # Add mutually exclusive arguments
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        '-p1',
        '--part-1',
        action='store_true',
        help='Prepare calendar data into CSV file'
    )
    group.add_argument(
        '-p2',
        '--part-2',
        action='store_true',
        help='Create Google calendar using existing CSV data file'
    )

    # Add remaining arguments
    parser.add_argument(
        '--input-csv',
        help='Calendar data file to be inputted',
        type=argparse.FileType('r'),
        metavar='IN'
    )
    parser.add_argument(
        '--output-csv',
        help='Calendar data file to be outputted',
        type=argparse.FileType('w'),
        metavar='OUT'
    )
    parser.add_argument(
        '-y',
        '--year',
        help='Calendar year',
        type=int
    )
    parser.add_argument(
        '-s',
        '--season',
        help='Calendar season',
        type=str
    )
    parser.add_argument(
        '-v',
        '--verbose',
        action='store_true',
        help='Increase output verbosity'
    )


    args = parser.parse_args()
    args_as_dict = vars(args)
    if args_as_dict['part_1']:
        if not args_as_dict['output_csv']:
            parser.error('-p1 flag requires a --output-csv flag')
        elif not args_as_dict['year']:
            parser.error('-p1 flag requires a --year flag')
        elif not args_as_dict['season']:
            parser.error('-p1 flag requires a --season flag')

        parse.perform(
            args_as_dict['output_csv'],
            args_as_dict['year'],
            args_as_dict['season']
        )
    elif args_as_dict['part_2']:
        if not args_as_dict['input_csv']:
            parser.error('-p2 flag requires a --input-csv flag')
        elif not args_as_dict['year']:
            parser.error('-p2 flag requires a --year flag')
        elif not args_as_dict['season']:
            parser.error('-p2 flag requires a --season flag')

        upload.perform(
            args_as_dict['input_csv'],
            args_as_dict['year'],
            args_as_dict['season']
        )
    else:
        if not args_as_dict['year']:
            parser.error('--year flag is required')
        elif not args_as_dict['season']:
            parser.error('--season season is required')

        combined.perform(
            args_as_dict['year'],
            args_as_dict['season']
        )

if __name__ == '__main__':
    handle_arguments()
