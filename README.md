# calscrape

A command line tool that creates a Google Calendar with data scraped
from the official CCNY academic calendar.

### Getting started

```bash
git clone https://gitlab.com/ian-s-mcb/calscrape
cd calscrape
pip install -r requirements.txt

# Download your Google API credentials
# - Visit: https://developers.google.com/calendar/quickstart/python
# - Click the "enable Google Calendar API" button
# - Download the credentials.json file
# - Move the .json file into this repo dir

# Run first part
./calscrape.py -p1 --output-csv data.csv -y 2020 -s spring

# Run second part
./calscrape.py -p2 --input-csv data.csv -y 2020 -s spring

# Or, alternatively, run the two parts in combination
./calscrape.py -y 2020 -s spring
```

#### Technologies used

* [Argparse][argparse]
* [Beautifulsoup][bs]
* [Google Calendar Python API][google-calendar]
* [Pandas][pd]
* [Requests][requests]

[argparse]: https://docs.python.org/3/library/argparse.html
[bs]: https://www.crummy.com/software/BeautifulSoup/bs4/doc/
[google-calendar]: https://developers.google.com/calendar/quickstart/python
[pd]: https://pandas.pydata.org/
[requests]: https://2.python-requests.org/en/master/
