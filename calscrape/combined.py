'''
combined.py

Provides a combined parse and upload operation.
'''
from . import parse
from . import upload

def perform(year, season):
    # Parse page
    page = parse.retreive_page(year, season)
    rows = parse.parse_page(page)
    df = parse.organize_data(rows, year, season)

    service = upload.build_service()

    # Delete and recreate calendar
    calendar_name = 'Academic2'
    upload.delete_calendar(service, calendar_name)
    cal_id = upload.insert_calendar(service, calendar_name)

    # Populate calendar with events
    upload.insert_events(df, service, cal_id)
