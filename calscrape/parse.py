'''
parse.py

Provides a parse operation, which involves the following steps:
requesing a page, parsing the page, and organizing the calendar data.
'''
import pandas as pd
import requests
import re
import datetime
from bs4 import BeautifulSoup

month_to_num = {
    'January': 1,
    'February': 2,
    'March': 3,
    'April': 4,
    'May': 5,
    'June': 6,
    'July': 7,
    'August': 8,
    'September': 9,
    'October': 10,
    'November': 11,
    'December': 12
}

num_to_month = {
    1: 'January',
    2: 'February',
    3: 'March',
    4: 'April',
    5: 'May',
    6: 'June',
    7: 'July',
    8: 'August',
    9: 'September',
    10: 'October',
    11: 'November',
    12: 'December'
}

def add_year_to_date(date, year_academic_calendar, season):
    date_month = date.split(' ')[0]
    date_month_num = month_to_num[date_month]
    if season == 'fall':
        year_actual = year_academic_calendar if date_month_num > month_to_num['July'] else str(int(year_academic_calendar) + 1)
    else:
        year_actual = year_academic_calendar if date_month_num < month_to_num['July'] else str(int(year_academic_calendar) - 1)
    return f'{date} {year_actual}'

def split_interval(interval, description, year):
    pre, post = map(str.strip, interval.split('–'))
    start_month = interval.split(' ')[0]
    if post.isdigit():
        dates = pd.date_range(
            start=f'{pre} {year}',
            end=f'{start_month} {post} {year}')
    else:
        dates = pd.date_range(
            start=f'{pre} {year}',
            end=f'{post} {year}')
    return pd.DataFrame(
        {'description': len(dates) * [description]},
        index=dates)

def retreive_page(year, season):
    url = f'https://www.ccny.cuny.edu/registrar/{season}-{year}-academic-calendar'
    req = requests.get(url)
    if req.status_code != 200:
        print(f'ERROR: Bad url {url}')
        exit(1)
    return req.text

def parse_page(page_text):
    soup = BeautifulSoup(page_text, 'html.parser')
    tables = soup.find_all('table')
    # Sanity check table
    if len(tables) != 1:
        print('ERROR: Too many tables found in page')
        exit(1)
    table = tables[0]
    table_rows = table.find_all('tr')
    header_tds = table_rows[0].find_all('td')

    # Sanity check table format
    if ((len(header_tds) != 3)
        or not (header_tds[0].find_all(string=re.compile('dates', flags=re.IGNORECASE)))
        or not (header_tds[1].find_all(string=re.compile('days', flags=re.IGNORECASE)))):
        print('ERROR: Unexpected table format\nThree columns are expected: dates, days, description')
        exit(1)

    # Remove header row of table
    table_body_rows = table_rows[1:]
    rows_by_td = [[td.find_all('p') for td in row.find_all('td')] for row in table_body_rows]

    # Select dirty rows
    # ie. ones that don't have exactly one p tag in each of the three td's of each row
    rows_dirty = [[td for td in row] for row in rows_by_td
                  if ((len(row[0]) != 1)
                      or (len(row[1]) != 1)
                      or (len(row[2]) != 1))]
    if len(rows_dirty):
        print(f'WARN: Number of dirty rows: {len(rows_dirty)}')
        for i, row in enumerate(rows_dirty):
            print(f'WARN: Row-{i}')
            for j, column in enumerate(row):
                print(f'WARN: \tColumn-{j} {column}')

    # Select clean rows
    rows_clean = [[td for td in row] for row in rows_by_td
                  if ((len(row[0]) == 1)
                      and (len(row[1]) == 1)
                      and (len(row[2]) == 1))]

    # Strip remaining tags and whitespace
    rows_clean_stripped = [[' '.join(map(str, list(td[0].stripped_strings))) for td in row] for row in rows_clean]

    return rows_clean_stripped

def organize_data(rows, year, season):
    # Reorient rows into lists inside a dict
    dates = [row[0] for row in rows]
    data = {
        'day': [row[1] for row in rows],
        'description': [row[2] for row in rows]
    }

    # Make dataframe from dict
    df = pd.DataFrame(data, index=dates)

    # Collect records that span only one day
    df_single_day_records = df[~df.index.str.contains('–')].copy()
    df_single_day_records.index = pd.to_datetime(
        df_single_day_records.index.map(
            lambda x: add_year_to_date(x, year, season)
        )
    )

    # Collect records that span multiple days
    df_multi_day_records = df[df.index.str.contains('–')].copy()

    # Break multi-day records into multiple rows
    df_multi_day_records_fixed = pd.DataFrame({'description': []}, index=[])
    for interval, description in zip(df_multi_day_records.index,
                                     df_multi_day_records['description']):
        # Split interval dates into multiple rows
        # Then concat resulting DataFrame
        df_multi_day_records_fixed = pd.concat([df_multi_day_records_fixed,
                                                split_interval(interval, description, year)],
                                              sort=False)

    # Add column for day name based on date column
    df_multi_day_records_fixed['day'] = pd.to_datetime(df_multi_day_records_fixed.index).day_name()

    # Combine, serialize all records
    df_full = pd.concat([df_single_day_records,
                         df_multi_day_records_fixed],
                        sort=False)
    df_full.sort_index(inplace=True)
    return df_full

def perform(output_file, year, season):
    page = retreive_page(year, season)
    rows = parse_page(page)
    data = organize_data(rows, year, season)
    data.to_csv(output_file)
    output_file.close()
