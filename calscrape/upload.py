'''
upload.py

Provides the upload operation, which involves the following steps:
building the Google API "service", deleting and recreating the calendar
(if it already exists), inserting events into the calendar.
'''
from __future__ import print_function
import datetime
import pickle
import os.path
import pandas as pd
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/calendar']

def build_service():
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('calendar', 'v3', credentials=creds)
    return service

def delete_calendar(service, calendar_name):
    # Get id of calendar to be deleted
    req_results = service.calendarList().list().execute()
    calendars = req_results.get('items')
    cal_details = [cal
        for cal in calendars
        if cal['summary'] == calendar_name]
    if not cal_details:
        print(f'WARN: The calendar "{calendar_name}" cannot be deleted '
            'because it does not exist')
        return
    cal_id = cal_details[0]['id']

    # Delete calendar
    service.calendars().delete(calendarId=cal_id).execute()
    print(f'INFO: deleted calendar - name: {calendar_name}')

def insert_calendar(service, calendar_name):
    calendar = {
        'summary': calendar_name,
        'timeZone': 'America/New_York'
    }
    created_calendar = service.calendars().insert(
        body=calendar
    ).execute()
    
    print(f'INFO: inserted calendar - name: {calendar_name}')
    return created_calendar['id']

def insert_events(df, service, cal_id):

    # Create a column with abbreviated event names
    def trim(s):
        if len(s) > 20:
            return f'{s[:20]}...'
        else:
            return s
    df['abbrev'] = df.description.apply(trim)
    
    # Cast index as a string
    df.index = df.index.astype(str)
    
    # Perform insertion
    for index, row in df.iterrows():
        event = {
            'summary': row['abbrev'],
            'description': row['description'],
            'start': {'date': index},
            'end': {'date': index}
        }  
        created_event = service.events().insert(
            calendarId = cal_id,
            body = event
        ).execute()
        print(f'INFO: inserted event - start: {index}, summary: {row["abbrev"]}')
    print(f'INFO: inserted a total of {len(df)} events')
    
def perform(input_file):
    service = build_service()

    # Delete and recreate calendar
    calendar_name = 'Academic'
    delete_calendar(service, calendar_name)
    cal_id = insert_calendar(service, calendar_name)

    # Populate calendar with events
    df = pd.read_csv(input_file, index_col=0)
    insert_events(df, service, cal_id)
